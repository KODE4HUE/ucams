<?php  namespace App\Services\Asset;

use App\Repositories\Depreciation\DepreciationMethod\IDepreciationMethodRepository;
use App\Services\ServiceManager;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;



class AssetServiceManager extends ServiceManager {
    
    protected $depreciationMethodRepository;
    protected $assetModelRepository;
    protected $assetRepository;
    protected $createAssetValidation;


    public function __construct( 
        IDepreciationMethodRepository $depreciationMethodRepository,
        \App\Repositories\Asset\AssetModel\IAssetModelRepository $assetModelRepository,
        \App\Repositories\Asset\Asset\IAssetRepository $assetRepository,
        \App\Services\Asset\Validation\CreateAssetValidation $createAssetValidation

    )
    {
        $this->depreciationMethodRepository = $depreciationMethodRepository;
        $this->assetModelRepository = $assetModelRepository;
        $this->assetRepository = $assetRepository;
        $this->createAssetValidation = $createAssetValidation;
    }


    public function getAllAssets()
    {
        $hasData = false;

        $result = $this->assetRepository->all();

        $errorMessages = new MessageBag();

        if($result['error'])
        {
            $result['error-messages']['server-error'] = "There was an error trying to retrieve the specified resource";
            $hasData = false;
        }
        else
        {
            if(!$result['data'])
            {
                $errorMessages->add('custom', "No user found");
                $result['error-messages']['validation-messages'] = $errorMessages;
                $hasData = false;
            }
            else
            {
                $hasData = true;
            }
        }

        $this->result = $result;


        return $hasData;
    }


    public function createAsset($data)
    {
        $created = false;

        try{

            dd('here');

            // get currently logged in user information
            $currentlyLoggedInUserResult = $this->authenticationRepository->getLoggedInUser();

            if($currentlyLoggedInUserResult['error'])
            {
                throw new Exception('Could not retrieve currently logged in user');
            }

            if(isset($data['additional_details']))
            {
                if(empty($data['additional_detail']))
                {
                    $data['additional_details'] = "";
                }
                else
                {
                    $data['additional_details'] = "";
                }
            }
            else
            {
                $data['additional_details'] = "";
            }


        }
        catch(Exception $e)
        {
            Log::error('There was a database error in the ' . __CLASS__ . ' class, on line number [' . __LINE__ . '] of ' . __METHOD__ . 'method');
            Log::debug($e->getTraceAsString());

            $this->result['successs-message'] = false;
            $this->result['error'] = true;
            $this->result['error-messages']['server-error'] = "Unable to save asset model";
            $created = false;

        }


        return $created;

    }
    
    public function getDepreciationMethodSelectable($displayName = 'depreciation_method_name')
    {
        $hasData = false;

        
         $dbResult = $this->depreciationMethodRepository->getSelectable($displayName);
        
        if($dbResult['error'])
        {
            $this->result['error-messages']['server-error'] = "There was an error trying to retrieve the specified resources";
            $hasData = false;
        }
        else
        {
             
            if(!$dbResult['data'])
            {
                $this->result["error"]  = true;
                $this->result['error-messages']['not-found'] =  "No results found";
                $hasData = false;  
            }
            else
            {
                $this->result['data'] = $dbResult['data'];
                $this->result["success-message"] = "successfully retrieved list";
                $hasData = true;
            }
        }
        
        return $hasData;
        
    }
   
}
