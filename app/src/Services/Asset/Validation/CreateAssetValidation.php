<?php  namespace App\Services\Asset\Validation;

namespace App\Services\Asset\Validation;
use App\Services\CustomValidation;

class CreateAssetValidation extends  CustomValidation{

    protected  static $rules = [
        'asset_model_id'                  => 'required',
        'asset_lifecycle_stages_id'                => '',
        'serial_number'      => 'required',
        'ledger_code'               => '',
        'cost_price'                => 'required',
        'current_value'                 => 'required',
        'total_tax'                 => '',
        'utech_code'                => '',
        'is_disposed'               => '',
        'date_disposed'             => '',
        'purchased_on'              => '',
        'assigned_to_staff_id'      => '',
        'owner_school_id'           => '',

        'agreement_type_id'         => 'required',
        'transaction_id'            => 'required',
        'additional_details'        => ''

    ];

    protected $messages = [
        'asset_model_id.required'      => 'The asset model is required',
        'asset_model_id.exists'         => 'The asset model selected does not exist',
        'serial_number.required'       => 'The serial number is required'
    ];

}