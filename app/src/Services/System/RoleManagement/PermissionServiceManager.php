<?php namespace App\Services\System\RoleManagement;

use App\Repositories\System\RoleManagement\Permission\IPermissionRepository;
use App\Repositories\System\RoleManagement\PermissionRole\IPermissionRoleRepository;
use App\Services\ServiceManager;
use App\Services\System\RoleManagement\Validation\permissionValidation;
use App\Services\System\RoleManagement\Validation\PermissionWithRoleValidation;
use Illuminate\Support\MessageBag;

/**
 * Description of PermissionServiceManager
 *
 * @author hue
 */
class PermissionServiceManager extends ServiceManager
{

    protected $permissionRepository;
    protected $permissionValidation;
    protected $permissionRoleRepository;
    protected $permissionWithRoleValidation;
    
    public function __construct( 
            IPermissionRepository $permissionRepository, 
            permissionValidation $permissionValidation,
            IPermissionRoleRepository $permissionRoleRepository,
            PermissionWithRoleValidation $permissionWithRoleValidation
    )
    {
        $this->permissionRepository = $permissionRepository;
        $this->permissionValidation = $permissionValidation;
        $this->permissionRoleRepository = $permissionRoleRepository;
        $this->permissionWithRoleValidation = $permissionWithRoleValidation;
    }
    
    public function createPermission($data)
    {
        $created = false;
                
        if($this->permissionValidation->passes($data))
        {
            
            $data['name'] = strtolower( str_replace(' ', '_', $data['display_name']));
            
            $this->result = $this->permissionRepository->create($data);
            
            if(!$this->result['error'])
            {
                $created = true;
            }   
        }
        else
        {
            $created =false;
            
            $this->result['data'] = false;
            $this->result['error'] = true;
            $this->result['error-messages']['validation-errors'] = $this->createPermissionValidation->getErrors();
            
        }
       
        return $created;
    }
    
    public function getAllPermissions()
    {
        $hasData = false;
        $this->resetResult();
        
        $errorMessages = new MessageBag();
        
        $dbResult = $this->permissionRepository->all();
        
        if($dbResult['error'])
        {
            $this->result['error-messages']['server-error'] = "There was an error trying to retrieve the specified resource";
            $hasData = false;
        }
        else
        {
            
            if(!$dbResult['data'])
            {
                $this->result["error"]  = true;
                $this->result['error-messages']['not-found'] = "No results found";
                $hasData = false;    
            }
            else
            {
                $this->result["success-message"] = "successfully fetched record";
                $this->result['data'] = $dbResult['data'];
                $hasData = true;
            }
        }
        
        return $hasData;
    }
    
    public function getPermission($id)
    {
        $hasData = false;
        $this->resetResult();
        
        $dbResult = $this->permissionRepository->get($id);
        
        if($dbResult['error'])
        {
            $this->result['error-messages']['server-error'] = "There was an error trying to retrieve the specified resource";
            $hasData = false;
        }
        else
        {
             
            if(!$dbResult['data'])
            {
                
                $this->result["error"]  = true;
                $this->result['error-messages']['not-found'] =  "No results found";
                $hasData = false;  
            }
            else
            {
                $this->result['data'] = $dbResult['data'];
                $this->result["success-message"] = "successfully fetched record";
                $hasData = true;
            }
        }
        
        return $hasData;
    }
    
    public function updatePermission($data)
     {
         $updated = false;
         $this->resetResult();
         
         if($this->permissionValidation->passes($data))
         {
             
            if($this->permissionRepository->exists($id))
            {
                
                $data['name'] = strtolower( str_replace(' ', '_', $data['display_name']));

                $dbResult = $this->permissionRepository->update($data);

                if(!$dbResult['error'])
                {
                    $this->result['success-message'] = "Successfully updated resource";
                }
                else
                {
                    $this->result['error-messages']['server-error'] = "There was a problem while updating the specified resource";
                }
                
            }
            else
            {
                $updated = false;
            
                $this->result['error-messages']['not-found'] = "The specified resource does not exist";
                $this->result['error'] = true;
                
            }
             
         }
         else
         {
            $updated =false;
            
            $this->result['error-messages']['validation-messages'] = $this->permissionValidation->getErrors();
            $this->result['error'] = true;
           
         }
          
         return $updated;
     }
     
    public function archivePermissionById($id)
    {
        $archived = false;
        $this->resetResult();
        
        //check if item does not exist or is already archived
        if($this->permissionRepository->exists($id))
        {
            
            $dbResult = $this->permissionRepository->archive($id);
            
            if(!$dbResult['error'])
            {
                $archived = true;
                $this->result['success-message'] = "Successfully archived permission";
                $this->result['error'] = true;
            }
            else
            {
                $archived = false;
                $this->result['error-messages']['server-error'] = "Unable to archive permission";
            }
             
        }
        else
        {
            // the specified resourse could not be found
            $archived =false;
            
            $this->result['error'] = true;
            $this->result['error-messages']['not-found'] = "The specified resource could be archived because it does not exist";
        }
        
        return $archived;
    }
   
    public function associatePermissionWithRole($roleId, $permissionId)
    {
        $created = false;
        $this->resetResult();
        $validationPassed = true;
        $validationErrorMessages = new MessageBag();
        
        try
        {
            
            $data = [
                'permission_id' => $permissionId, 
                'role_id'       => $roleId 
            ];
            
            if(!$this->permissionWithRoleValidation->passes($data))
            {
                $validationPassed = false;
                $validationErrorMessages = $this->permissionWithRoleValidation->getErrors();
            }

            if($this->permissionRoleRepository->existsWhere($data))
            {
                $validationPassed = false;
                $validationErrorMessages->add('custom', 'The permission is already associated with the role specified');

            }

            if($validationPassed)
            {
                $dbResult = $this->permissionRoleRepository->create($data);

                if(!$dbResult['error'])
                {
                    $this->result['success-message'] = "Permission successfully added to role";
                    $created = true;
                }
                else{
                    $this->result['error-messages']['server-error'] = "Unable to assoicate permission with role";
                    $this->result['error'] = true;
                    $created = false;
                }
            }
            else
            {
                $this->result['error-messages']['validation-messages'] = $validationErrorMessages;
                $this->result['error'] = true;
                $created = false;
            }
        }   
        catch (Exception $ex) 
        {

            Log::error('There was a service error in the ' . __CLASS__ . ' class, on line number [' . __LINE__ . '] of ' . __METHOD__ . 'method');
            Log::debug($e->getTraceAsString());
            
            $this->result['data'] = false;
            $this->result['error-messages']['server-error'] = "Unable to assoicate permission with role";
            $this->result['error'] = true;
            $created = false;
        }
        
        return $created;
        
    }
    
    public function deassociatePermissionWithRole( $roleId, $permissionId)
    {
        $deassociated = false;
        $validationPassed = true;
        $validationErrorMessages = new MessageBag();
        
        $data =  [
            'role_id',
            'permission_id'
        ];
        
        if(!$this->permissionWithRoleValidation->passes($data))
        {
            $validationPassed = false;
            $validationErrorMessages = $this->permissionWithRoleValidation->getErrors();
        }
        
        if(!$this->permissionRoleRepository->existsWhere($data))
        {
            $validationPassed = false;
            $validationErrorMessages->add('custom', 'The permission is not associated with the role specified');
             
        }
        
        if($validationPassed)
        {
            $result = $this->permissionRoleRepository->delete($data);
            
            if(!$result['error'])
            {
                $created = true;
            }
        }
        else
        {
            $result['error-messages']['validation-messages'] = $validationErrorMessages;
            $created = false;
        }
        
        $this->result = $result;
        
        
        return $deassociated;
    }
    
}
