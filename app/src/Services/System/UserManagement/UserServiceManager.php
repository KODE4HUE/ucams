<?php  App\Services\System\UserManagement;

use App\Repositories\System\UserManagement\User\IUserRepository;
use App\Services\ServiceManager;

/**
 * Description of UserServiceManager
 *
 * @author hubert
 */
class UserServiceManager extends ServiceManager
{
    protected $createUserValidation;
    protected $userRepository;
    
    public function __construct(IUserRepository $userRepository, createUserValidation $createUserValidation)
    {
        $this->createUserValidation = $createUserValidation;
        $this->userRepository = $userRepository;
       
    }
    
    public function createUser($data)
    {
        $created = false;
        
        if($this->createUserValidation->passes()){
            $result = $this->userRepository->create($data);
            
            if(!$result['error'])
            {
                $created= true;
            }
            else
            {
                $result['error-messages']['server-error'] = "Unable to create user";
                $created = false;
            }
        }
        else
        {
            $created =false;
            
            $result = [
                'data'            => false,
                'status-code'     => 401,
                'error'           => true,
            ];

            $result['error-messages']['validation-messages'] = $this->roleValidation->getErrors();
        }
        
        $this->result = $result;
        
        return $created;  
    }
 
    
    public function getAllUsers()
    {
        $hasData = false;
        $result = $this->userRepository->all();
        
        $errorMessages = new MessageBag();
        
        if($result['error'])
        {
            $result['error-messages']['server-error'] = "There was an error trying to retrieve the specified resource";
            $hasData = false;
        }
        else
        {
            if(!$result['data'])
            {
                $errorMessages->add('custom', "No user found");
                $result['error-messages']['validation-messages'] = $errorMessages;
                $hasData = false;
            }
            else
            {
                $hasData = true;
            }
        }
        
        $this->result = $result;
        
        return $hasData;
    }
    
    public function getUser($id)
    {
         $hasData = false;
       
        $result = $this->userRepository->get($id);
        
        if(!$result['error'])
        {
            if(!$result['data'])
            {
                $result['error-messages']['validation-error'] = new MessageBag("custom", "The user was not found");
                $hasData = false;
            }
            else
            {
                $hasData= true;
            }
            
        }
        else
        {
            $hasData = false;
            $result['error-messages']['server-error'] = "There was an error trying to retrieve the specified resource";
            
        }
        
        $this->result = $result;
        
        return $hasData;
    }
    
    public function updateUser($data)
    {
        
    }
}
