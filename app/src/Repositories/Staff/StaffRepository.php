<?php namespace App\Repositories\Staff;

use App\Repositories\BaseRepository;
use Model\Staff\Staff;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StaffRepository extends BaseRepository implements IStaffRepository 
{
    
    protected $staff;
    
    public function __construct(Staff $staff, $_sortByColumn = false, $_sortByOrder = "DESC", $_itemPerPage=25)
    {
        parent::__construct($staff, $_sortByColumn, $_sortByOrder, $_itemPerPage);
        $this->staff = $staff;
    }


    public function all(array $related = null)
    {
        $result['data'] = false;
        $result['error'] = false;

        try
        {
            $result['data'] = DB::table('staff')->get();

        }
        catch (Exception $e)
        {

            Log::error('There was a database error in the ' . __CLASS__ . ' class, on line number [' . __LINE__ . '] of ' . __METHOD__ . 'method');
            Log::debug($e->getTraceAsString());

            $result['error'] = true;
            $result['error-messages'] = new MessageBag(['server-error'  => "There was an error trying to fetch the specified resources"]);
        }

        return $result;

    }
}
