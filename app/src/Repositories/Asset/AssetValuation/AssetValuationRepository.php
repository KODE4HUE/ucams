<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Repositories\Asset\AssetValuation;

use App\Repositories\BaseRepository;
use Model\Asset\Asset_Valuation;

/**
 * Description of AssetValuationRepository
 *
 * @author hue
 */
class AssetValuationRepository extends BaseRepository implements IAssetValuationRepository {

    //put your code here
    
    public function __construct(Asset_Valuation $assetValuation, $_sortByColumn = false, $_sortByOrder = "DESC", $_itemPerPage)
    {
        parent::__construct($model, $_sortByColumn, $_sortByOrder, $_itemPerPage);
    }
}
