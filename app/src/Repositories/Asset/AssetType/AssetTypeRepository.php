<?php namespace App\Repositories\Asset\AssetType;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of AssetTypeRepository
 *
 * @author hue
 */
class AssetTypeRepository extends \App\Repositories\BaseRepository implements IAssetTypeRepository {

    
    public function __construct(AssetType $assetType, $_sortByColumn = false, $_sortByOrder = "DESC", $_itemPerPage)
    {
        parent::__construct($model, $_sortByColumn, $_sortByOrder, $_itemPerPage);
    }
}
