<?php namespace App\Repositories\System\RoleManagement\Permission;

use App\Repositories\BaseRepository;
use Model\System\RoleManagement\Permission;

class PermissionRepository extends BaseRepository implements IPermissionRepository {

    protected $permission;
    
    public function __construct(Permission $permission, $_sortByColumn = false, $_sortByOrder = "DESC", $_itemPerPage=25)
    {
        parent::__construct($permission, $_sortByColumn, $_sortByOrder, $_itemPerPage);
        $this->permission = $permission;
    }
}
