<?php namespace App\Repositories\System\Authentication;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * Description of AuthenticationRepository
 *
 * @author hue
 */
class AuthenticationRepository implements IAuthenticationRepository 
{

    
    public $currentlyLoggedInUser = false;
    /*
     * Method to retrieve/generate the authentication token for
     * a valid user 
     * @params $credentials - array with username and password
     */
    public function generateAuthenticationToken($credentials)
    {
        
        $result['data'] = false;
        $result['error'] = false;
      
        
        $token =false;
        
        try
        {              
            
            /**
             * set confirmed to 1 to ensure the user has to confirm via email
             * before their accounts can be confirmed
             */
            $credentials['confirmed'] = 1;
            
            $token = JWTAuth::attempt($credentials);           

            if (!$token)
            {
                $result['data'] =false;
                $result['error'] =true;
                $result['error-messages'] =  new MessageBag(['server-error'  => 'Invalid credentials']);
            }
            else
            {
                
                // set the authenticated user from the token
                if (!$this->setAuthenticatedUserFromToken($token))
                {
                    throw new Exception("unable to set the current authenticated user via token");
                }
                
                $result['data'] = [
                    'success-message' => 'Successfully logged in',
                    'token'   =>  $token
                ];
                
                $result['error'] = false;

            }
            
        } catch (Exception $e) 
        {
            
            dd($e->getMessage());
            // log exception
            Log::error('There was a database error in the ' . __CLASS__ . ' class, on line number [' . __LINE__ . '] of ' . __METHOD__ . 'method');
            Log::debug($e->getTraceAsString());
            
            $result['error'] = true;
            $result['error-messages'] =  new MessageBag(['server-error'  => 'There was an error while trying to authenticate the user']);
           
        }

        return $result;
    }
    
    
    /*
     * method to retrieve the currently logged in user via token of
     * logged in user submitting the request
     * @params
     * returns an array containing the error messsages and data if present
     */
    public function getLoggedInUser()
    {
        $result['data'] = false;
        $result['error'] = false;
        $validationErrorMessages = new MessageBag();
        $formattedUserProfileInfo = [];
        
         try 
        {

            if (! $user = JWTAuth::parseToken()->toUser())
            {
                // user not found
                $result['data'] = false;
                $result['error'] = true;
                $result['error-messages']['not-found'] = "The current user information could not be retrieved";
            }
            else
            {
                // user found
                unset($user['password']);
                unset($user['created_at']);
                unset($user['updated_at']);
                unset($user['deleted_at']);
                
                // fetch staff information with staff roles
                $select = DB::table('staff')->where('staff.id', '=', $user['staff_id']);
                $select = $select->join('ref_gender','staff.gender_id', '=', 'ref_gender.id');
                $staffProfile = $select->first([
                                    'staff.id as staff_id',
                                    'first_name', 
                                    'last_name',
                                    'job_description', 
                                    'profile_image', 
                                    'cell_phone_number',
                                    'staff.address_line',
                                    'staff.gender_id'
                                ]);
                
                unset($staffProfile['created_at']);
                unset($staffProfile['updated_at']);
                unset($staffProfile['deleted_at']);
                
                $select =   DB::table('assigned_role')->where('assigned_role.user_id', '=', $user['id']);
                $select = $select->join('role', 'role.id', '=', 'assigned_role.role_id');
                $select = $select->select(
                            'assigned_role.role_id',
                            'role.name as role_name',
                            'description'
                          );
                
                $loggedInUserRoles = $select->get();
                
                // dd($staffProfile);
              
                // format user profile data
                $formattedUserProfileInfo = array_merge($user->attributesToArray(), $staffProfile);
                        
                foreach ($loggedInUserRoles as $rolesItem)
                {
                   
                     $formattedUserProfileInfo['roles'][] = $rolesItem;
                }
                
                $result['data'] = $formattedUserProfileInfo;
               
            }

        } 
        catch (TokenExpiredException $e) 
        {
            // token expired
            
            $this->result['data'] = false;
            $this->result['error'] = true;
            $validationErrorMessages->add('token-expired', 'The user session has expired');
            $this->result['error-messages']['validation-messages'] = $validationErrorMessages->getMessages();
            $hasCurrentStaff = false;

        } 
        catch (TokenInvalidException $e) 
        {
            // token invalid
  
            $this->result['data'] = false;
            $this->result['error'] = true;
            $validationErrorMessages->add('token-invalid', 'The user token provided is invalid');
            $this->result['error-messages']['validation-messages'] = $validationErrorMessages->getMessages();

        } 
        catch(Exception $e)
        {
            // regular exception
            
            Log::error('There was a service error in the ' . __CLASS__ . ' class, on line number [' . __LINE__ . '] of ' . __METHOD__ . 'method');
            Log::debug($e->getTraceAsString());
            
            $this->result['data'] = false;
            $this->result['error'] = true;
            $this->result['error-messages']['server-error'] = "Error trying to retrieve current user profile information information"; 
        }
        
        return $result;
        
    }
    
    
    /*
     * Sets the currently logged in user from token passed
     * @params $token
     */
    protected function setAuthenticatedUserFromToken($token)
    {
        $authenticatedUserSetSuccessfully = false;
        
        JWTAuth::setToken($token);
        
        $user = JWTAuth::toUser();
        
        if ($user )
        {
            
            $user = $user->toArray();
            
            unset($user['created_at']);
            unset($user['updated_at']);
            unset($user['deleted_at']);
                      
            $this->currentlyLoggedInUser =$user;
            $authenticatedUserSetSuccessfully = true;
        }
        
        return $authenticatedUserSetSuccessfully;
    }
    
    
    /*
     * Retrieves the currently logged in user 
     */
    public function getCurrentUser()
    {
        if($this->currentlyLoggedInUser)
        {
            return $this->currentlyLoggedInUser;
        }
        else{
            return false;
        }
    }
    
    /*
     * Reset the currently logged in user to false
     */
    protected function resetCurrentUser()
    {
        
        $this->currentlyLoggedInUser = false;
    }
    
}
