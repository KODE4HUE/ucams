<?php namespace App\Repositories\System\UserManagement\User;

interface IUserRepository
{
    public function update(array $data);
}
