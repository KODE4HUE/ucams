<?php namespace App\Repositories\Depreciation\DepreciationMethod;
use App\Repositories\Depreciation\DepreciationMethod\IDepreciationMethodRepository;
use Model\Depreciation\DepreciationMethod;

use App\Repositories\BaseRepository;

/**
 * Description of DepreciationMethodRepository
 *
 * @author hue
 */
class DepreciationMethodRepository extends BaseRepository implements IDepreciationMethodRepository {

    //put your code here
    protected $depreciationMethod;
    
    public function __construct(DepreciationMethod $depreciationMethodModel, $_sortByColumn = false, $_sortByOrder = "DESC", $_itemPerPage = 25)
    {
        parent::__construct($depreciationMethodModel, $_sortByColumn, $_sortByOrder, $_itemPerPage);
        $this->depreciationMethod = $depreciationMethodModel;
    }
}
