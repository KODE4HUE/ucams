<?php namespace Model\Depreciation;

class DepreciationMethod extends \Illuminate\Database\Eloquent\Model 
{
    
    protected $table = 'depreciation_method';
    
    protected $fillable = [
      'depreciation_method_name',
      'depreciation_method_code',
      'description'  
    ];
    
}
