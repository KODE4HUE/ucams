<?php namespace Model\Asset;

use Illuminate\Database\Eloquent\Model;

class Asset extends UuidModel
{

    protected $table = 'asset';
    
     protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at'
    ];
     
    protected $fillable = [
        'asset_model_id',
        'asset_lifecycle_stages_id',
        'serial_number',
        'ledger_code',
        'cost_price',
        'current_value',
        'total_tax',
        'utech_code',
        'is_dispose',
        'date_disposed',
        'estimated_years_to_depreciate',
        'assigned_room_id',
        'assigned_to_staff_id',
        'owner_school_id',
        'agreement_type_id',
        'transaction_id',
        'created_by_user_id',
        'deleted_by_user_id',
        'created_at',
        'updated_at',
        'deleted_at'
        
    ];
}
