<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Model\System\RoleManagement\Role;
use Model\System\User;
use Rhumsaa\Uuid\Uuid;

class UserTableSeeder extends Seeder
{

    public function run()
    {

        /*----------------------------------------------------------*/
        /*--------User 1 with all roles------------------------------*/
        /*------------------------------------------------------------*/
        
        $email = 'hubert.graham90@gmail.com';
        
        DB::table('user')->insert([
            'id'       => Uuid::uuid4(),
            'username' => '1000067',
            'email' => $email,
            'password' => Hash::make('password'),
            'confirmed' => true
        ]);
        
        $insertedUser = DB::table('user')->where("email", "=", $email)->first();
          
        
        $adminRole = Role::where('name', '=', 'Administrator')->firstOrFail();
        $accountsRole = Role::where('name', '=', 'Accounts Personnel')->firstOrFail();
        $securityRole = Role::where('name', '=', 'Security Personnel')->firstOrFail();
        
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i s');
        
        $assignAdminRoleData = [
            'user_id'       =>  $insertedUser['id'],
            'role_id'       =>  $adminRole['id'],
            'created_at'    =>  $date,
            'updated_at'    =>  $date
        ];
        
         $assignAccountsRoleData = [
            'user_id'       =>  $insertedUser['id'],
            'role_id'       =>  $accountsRole['id'],
            'created_at'    =>  $date,
            'updated_at'    =>  $date
        ];
         
        $assignSecurityRoleData = [
            'user_id'       =>  $insertedUser['id'],
            'role_id'       =>  $securityRole['id'],
            'created_at'    =>  $date,
            'updated_at'    =>  $date
        ];
        
        // assign roles
        DB::table('assigned_role')->insert($assignAdminRoleData);
        DB::table('assigned_role')->insert($assignSecurityRoleData);    
        DB::table('assigned_role')->insert($assignAccountsRoleData);
        
        // create new staff profile
        
        $staffId = Uuid::uuid4();
        
        DB::table('staff')->insert([
            'id'       => $staffId,
            'staff_number' => '1000067',
            'email' => $email,
            'first_name' => 'Hubert',
            'last_name'  => 'Graham',
            'job_description'  => 'Software Developer',
            'profile_image'  => '',
            'address_line'  => '23 Care Avenue',
            'gender_id'   => 1,
            'created_at'  => date('Y-m-d H:i:s'),
            'updated_at'  => '0000-00-00 00:00:00',
            'deleted_at'  => '0000-00-00 00:00:00'
        ]);
        
        // update staff
        DB::table('user')
            ->where('username', '1000067')
            ->update(array('staff_id' => $staffId));


        /*----------------------------------------------------------*/
        /*--------User 2 with all Accounts roles----------------------*/
        /*------------------------------------------------------------*/

        $email = 'chantelpowise@hotmail.com';
        
        DB::table('user')->insert([
            'id'       => Uuid::uuid4(),
            'username' => '1000045',
            'email' => $email,
            'password' => Hash::make('password'),
            'confirmed' => true
        ]);
        
        $insertedUser = DB::table('user')->where("email", "=", $email)->first();
        
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i s');
        
         $assignAccountsRoleData = [
            'user_id'       =>  $insertedUser['id'],
            'role_id'       =>  $accountsRole['id'],
            'created_at'    =>  $date,
            'updated_at'    =>  $date
        ];
        
        // assign accounts role to user 2
        DB::table('assigned_role')->insert($assignAccountsRoleData);
        
        // create new staff profile for user 2
        
        $staffId = Uuid::uuid4();
        
        DB::table('staff')->insert([
            'id'       => $staffId,
            'staff_number' => '1000045',
            'email' => $email,
            'first_name' => 'Subrina',
            'last_name'  => 'Powise',
            'job_description'  => 'Accountant',
            'profile_image'  => '',
            'address_line'  => '23 Care Avenue',
            'gender_id'   => 2,
            'created_at'  => date('Y-m-d H:i:s'),
            'updated_at'  => '0000-00-00 00:00:00',
            'deleted_at'  => '0000-00-00 00:00:00'
        ]);
        
        // update staff
        DB::table('user')
            ->where('username', '1000045')
            ->update(array('staff_id' => $staffId));


         /*----------------------------------------------------------*/
        /*--------User 3 with all Security  roles----------------------*/
        /*------------------------------------------------------------*/

        $email = 'Orlando@teambakari.com';
        
        DB::table('user')->insert([
            'id'       => Uuid::uuid4(),
            'username' => '1000033',
            'email' => $email,
            'password' => Hash::make('password'),
            'confirmed' => true
        ]);
        
        $insertedUser = DB::table('user')->where("email", "=", $email)->first();
        
        $date = new DateTime();
        $date = $date->format('Y-m-d H:i s');
        
        $assignSecurityRoleData = [
            'user_id'       =>  $insertedUser['id'],
            'role_id'       =>  $securityRole['id'],
            'created_at'    =>  $date,
            'updated_at'    =>  $date
        ];
        
        // assign accounts role to user 2
        DB::table('assigned_role')->insert($assignSecurityRoleData);
        
        // create new staff profile for user 3
        
        $staffId = Uuid::uuid4();
        
        DB::table('staff')->insert([
            'id'       => $staffId,
            'staff_number' => '1000033',
            'email' => $email,
            'first_name' => 'Orlando',
            'last_name'  => 'Whyte',
            'job_description'  => 'Security Manager',
            'profile_image'  => '',
            'address_line'  => '23 Care Vin Lane',
            'gender_id'   => 1,
            'created_at'  => date('Y-m-d H:i:s'),
            'updated_at'  => '0000-00-00 00:00:00',
            'deleted_at'  => '0000-00-00 00:00:00'
        ]);
    
        DB::table('user')
            ->where('username', '1000045')
            ->update(array('staff_id' => $staffId));




    }

}
