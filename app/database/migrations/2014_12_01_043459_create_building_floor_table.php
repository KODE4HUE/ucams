<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuildingFloorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
            Schema::create('building_floor', function(Blueprint $table){
             
                $table->increments('id');
                $table->integer('building_id')->unsigned()->default(0);
                $table->integer('floor_level_id')->unsigned()->default(0);
                $table->string('building_floor_code', 12)->default('');
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            //
            Schema::drop('building_floor');
	}

}
