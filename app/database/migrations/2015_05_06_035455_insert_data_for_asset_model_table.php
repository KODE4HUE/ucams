<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertDataForAssetModelTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		    DB::table('asset_model')->insert([
                   'id'                 		=> 1,
                   'asset_name'    				=> 'Apple 4s',
                   'asset_model_image'			=> '',
                   'asset_type_id'				=> '0',
                   'product_code'				=> '4s',
                   'depreciation_method_id'     => '1',
                   'manufacturer'				=> 'Apple Inc',
                   'description'				=> '',


        ]);

        DB::table('asset_model')->insert([
                   'id'                 		=> 2,
                   'asset_name'    				=> 'Microsoft Surface Pro 3',
                   'asset_model_image'			=> '',
                   'asset_type_id'				=> '0',
                   'product_code'				=> 'Pro 3',
                   'depreciation_method_id'     => '1',
                   'manufacturer'				=> 'Microsoft',
                   'description'				=> 'A Tablet convertable to a laptop',


        ]);

        DB::table('asset_model')->insert([
                   'id'                 		=> 3,
                   'asset_name'    				=> 'Dell Inspiron 15',
                   'asset_model_image'			=> '',
                   'asset_type_id'				=> '0',
                   'product_code'				=> 'Inspiron 15',
                   'depreciation_method_id'     => '1',
                   'manufacturer'				=> 'Microsoft',
                   'description'				=> 'A Tablet convertable to a laptop',


        ]);


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		DB::table('asset_model')->delete();
	}

}
