<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgreementTypeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
            Schema::create('agreement_type', function(Blueprint $table){
               $table->increments('id');
               $table->string('agreement_name', 80)->default('');
               $table->string('agreement_code', 30)->default('');
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
            Schema::drop('agreement_type');
	}

}
