<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplierTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
            Schema::create('supplier', function(Blueprint $table){
                
                $table->char('id', 36)->primary();
                $table->string('supplier_name', 128)->default('');
                $table->string('supplier_code', 50)->default('');
                $table->string('email',255)->default('');
                $table->string('address_line', 255)->default('');
                $table->string('country', 80)->default('');
                $table->string('telephone_number', 20)->default('');
                
                $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('updated_at')->default('0000-00-00 00:00:00');
                $table->softDeletes();

                $table->char('created_by_user_id', 36)->default('0');
                $table->char('updated_by_user_id', 36)->default('0');
                $table->char('deleted_by_user_id', 36)->default('0');
                    
            });
                
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

            Schema::drop('supplier');
	}

}
