<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetTransactionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
                Schema::create('asset_transaction', function(Blueprint $table)
                {
                    
                   $table->char('id', 36)->primary();
                   $table->date('transaction_date');
                   $table->integer('total_items')->unsigned()->default(0);
                   $table->decimal('total_cost', 16, 4)->default(0);
                   $table->boolean('completed')->default(false);
                   $table->string('transaction_details', 255)->default('');
                   $table->string('cheque_number', 50)->default('');
                   $table->char('supplier_id', 36)->default(0);
                   
                   $table->timestamp('created_at');
                   $table->timestamp('updated_at')->default('0000-00-00 00:00:00');
                   $table->softDeletes();
                   
                   $table->char('created_by_user_id', 36)->default('0');
                    $table->char('updated_by_user_id', 36)->default('0');
                    $table->char('deleted_by_user_id', 36)->default('0');
                   
                   
                });
                
	}
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
              Schema::drop('asset_transaction');
	}

}
