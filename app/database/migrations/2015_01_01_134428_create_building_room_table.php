<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuildingRoomTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
            Schema::create('building_room', function(Blueprint $table){
               $table->increments('id');
               $table->string('room_name',60)->default('');
               $table->string('room_code', 36)->default('');
               $table->integer('associated_floor_id')->unsigned()->default(0);
               $table->integer('room_type_id')->unsigned()->default(0);
               
               $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
               $table->timestamp('updated_at')->default('0000-00-00 00:00:00');
               $table->softDeletes();

               $table->char('created_by_user_id', 36)->default('0');
                $table->char('updated_by_user_id', 36)->default('0');
                $table->char('deleted_by_user_id', 36)->default('0');
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
            Schema::drop('building_room');
	}

}
