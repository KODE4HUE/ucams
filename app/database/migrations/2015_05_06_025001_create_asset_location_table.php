<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetLocationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('asset_location', function(Blueprint $table)
		{
			$table->increments('id');
			$table->char('asset_id', 36)->default('0');
			$table->string('location_type')->default('building'); // or Campus
			$table->integer('building_room_id')->unsigned()->default(0);
			$table->string('campus_location_details')->default('');
			$table->string('notes')->default('');

			$table->char('is_stock_taking', 1)->default('n'); // y -- yes  n - no
			$table->char('located_by_staff_id')->default(''); // the staff member who located the item

			$table->timestamp('located_on_date')->default('0000-00-00 00:00:00');
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default('0000-00-00 00:00:00');
            $table->softDeletes();

			$table->char('created_by_user_id', 36)->default('0');
            $table->char('updated_by_user_id', 36)->default('0');
            $table->char('deleted_by_user_id', 36)->default('0');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('asset_location');
	}

}
