<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefRoomTypeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
            Schema::create('ref_room_type', function(Blueprint $table){
               
                $table->increments('id');
                $table->string('room_type_name', 80)->default('');
                $table->string('description', 150)->default('');
                
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
            Schema::drop('ref_room_type');
	}

}
