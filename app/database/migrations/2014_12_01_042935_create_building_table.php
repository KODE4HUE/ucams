<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuildingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
            Schema::create('building', function(Blueprint $table){
                
                $table->increments('id');
                $table->string('building_name', 128)->default('');
                $table->string('bulding_acronym', 32)->default('');
                $table->string('building_code', 128)->default('');
            });
                
                
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
            Schema::drop('building');
	}

}
