<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
   
	public function up()
	{
		//
            Schema::create('asset', function(Blueprint $table){
                $table->char('id', 36)->primary();
                $table->char('asset_model_id', 36)->default(0);
                $table->integer('asset_lifecycle_stages_id')->unsigned()->default(0);
                $table->string('serial_number', 255)->default('');
                $table->string('ledger_code', 10)->default('');
                $table->decimal('cost_price',16,4 )->default(0.0000);
                $table->decimal('current_value', 16,4)->default(0.0000);
                $table->decimal('total_tax', 16 ,4)->default(0.0000);
                $table->char('utech_code',36)->default('');
                $table->integer('estimated_years_to_depreciate')->unsigned()->default(0);
                $table->date('purchased_on');
                $table->boolean('is_disposed');
                $table->date('date_disposed');
                $table->text('additional_details');
                $table->char('assigned_room_id', 36)->default(0);
                $table->char('assigned_to_staff_id', 36)->default(0);
                $table->integer('owner_school_id')->unsigned()->default(0);
                $table->integer('agreement_type_id')->unsigned()->default(0);
                $table->integer('agreement_id')->unsigned()->default(0);
                $table->char('transaction_id', 36)->default(0);
                
                $table->char('created_by_user_id',36)->default('0');
                $table->char('updated_by_user_id', 36)->default('0');
                $table->char('deleted_by_user_id',36)->default('0');
                
                $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('updated_at')->default('0000-00-00 00:00:00');
                $table->softDeletes();
                
           });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            Schema::drop('asset');
	}

}
