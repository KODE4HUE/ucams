<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertDataForSupplierTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		DB::table('supplier')->insert([
                   'id'                 => 1,
                   'supplier_name'    => 'Watts New',
                   'email'			  =>  'wattsnew@gmail.com',
                   'supplier_code'	  =>  'default-watts-new',
                   'address_line'	  =>  '23 Care Lane',
                   'country'		  =>  'Jamaica',
                   'telephone_number' =>  '18764543434',

        ]);

        DB::table('supplier')->insert([
                   'id'                 => 2,
                   'supplier_name'    => 'Hardware & Lumber',
                   'email'			  =>  'wattsnew@gmail.com',
                   'supplier_code'	  =>  'default-hardware-lumber',
                   'address_line'	  =>  '23 Care Lane',
                   'country'		  =>  'Jamaica',
                   'telephone_number' =>  '18764593434',
                   
        ]);

        DB::table('supplier')->insert([
                   'id'                 => 3,
                   'supplier_name'    => 'RealStar Electronics & Apliances',
                   'email'			  =>  'realstar@gmail.com',
                   'supplier_code'	  =>  'default-real-stars',
                   'address_line'	  =>  '23 Care Lane',
                   'country'		  =>  'Jamaica',
                   'telephone_number' =>  '18764443434',
                   
        ]);

        DB::table('supplier')->insert([
                   'id'                 => 4,
                   'supplier_name'    => 'Ab Furniture & Office Supplies',
                   'email'			  =>  'ab@yahoo.com',
                   'supplier_code'	  =>  'default-ab-supplies',
                   'address_line'	  =>  '23 Care Lane',
                   'country'		  =>  'Jamaica',
                   'telephone_number' =>  '18764443431',
                   
        ]);

        DB::table('supplier')->insert([
                   'id'                 => 5,
                   'supplier_name'    => 'Jb Constructions & Building',
                   'email'			  =>  'jbconstruct@yahoo.com',
                   'supplier_code'	  =>  'default-jb-construct',
                   'address_line'	  =>  '23 Mathews Lane',
                   'country'		  =>  'Jamaica',
                   'telephone_number' =>  '18766573434',
                   
        ]);

        DB::table('supplier')->insert([
                   'id'                 => 6,
                   'supplier_name'    => 'Nissan Motors',
                   'email'			  =>  'nissanmotors@yahoo.com',
                   'supplier_code'	  =>  'default-nissan-motors',
                   'address_line'	  =>  '23 Mathews Lane',
                   'country'		  =>  'Jamaica',
                   'telephone_number' =>  '18766573334',
                   
        ]);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		 DB::table('supplier')->delete();
	}

}
