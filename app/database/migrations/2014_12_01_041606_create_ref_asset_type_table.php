<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefAssetTypeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
            Schema::create('ref_asset_type', function(Blueprint $table){
               
                $table->increments('id');
                $table->string('asset_type_name', 80)->default('');
                $table->string('description', 300)->default('');
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
            Schema::drop('ref_asset_type');
	}

}
