<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetModelTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

            Schema::create('asset_model', function(Blueprint $table)
            {
                $table->char('id', 36)->primary();
                $table->string('asset_name', 128)->default('');
                $table->string('product_code',128)->default('');
                $table->binary('asset_model_image');
                $table->integer('depreciation_method_id')->unsigned()->default(0);
                $table->integer('asset_type_id')->unsigned()->default(0);
                $table->string('manufacturer', 255)->default('');
                $table->string('description', 255)->default('');
                
                $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('updated_at')->default('0000-00-00 00:00:00');
                $table->softDeletes();
                
                $table->char('created_by_user_id', 36)->default('0');
                $table->char('updated_by_user_id', 36)->default('0');
                $table->char('deleted_by_user_id', 36)->default('0');
                
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
            Schema::drop('asset_model');
	}

}
