<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertDataForSchoolTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		DB::table('school')->insert([
                   'id'             		=> 1,
                   'school_name'			=> 'Caribbean School of Nursing',
                   'school_code'			=> 'CSN',
                   'parent_faculty_id'		=>  1,


        ]);

        DB::table('school')->insert([
                   'id'             => 2,
                   'school_name'	=> 'School of Allied Heath & Wellness',
                   'school_code'	=> 'CSN',
                   'parent_faculty_id'		=>  1,
	

        ]);

        DB::table('school')->insert([
                   'id'             => 3,
                   'school_name'	=> 'School of Pharmacy',
                   'school_code'	=> 'CSN',
                   'parent_faculty_id'		=>  1,
	

        ]);


        DB::table('school')->insert([
                   'id'             		=> 4,
                   'school_name'			=> 'Caribbean School of Nursing',
                   'school_code'			=> 'CSN',
                   'parent_faculty_id'		=>  1,


        ]);

        DB::table('school')->insert([
                   'id'             => 5,
                   'school_name'	=> 'School of Allied Heath & Wellness',
                   'school_code'	=> 'CSN',
                   'parent_faculty_id'		=>  1,
	

        ]);

        DB::table('school')->insert([
                   'id'             => 6,
                   'school_name'	=> 'School of Pharmacy',
                   'school_code'	=> 'CSN',
                   'parent_faculty_id'		=>  1,
	

        ]);


        DB::table('school')->insert([
                   'id'             => 7,
                   'school_name'	=> 'School of Technical & Vocational Education',
                   'school_code'	=> 'SOTFV',
                   'parent_faculty_id'		=>  3,
	

        ]);

        DB::table('school')->insert([
                   'id'             => 8,
                   'school_name'	=> 'Department of Liberal Studies',
                   'school_code'	=> 'DOLS',
                   'parent_faculty_id'		=>  3,
	

        ]);


        DB::table('school')->insert([
                   'id'             => 9,
                   'school_name'	=> 'School of Natural & Applied Sciences',
                   'school_code'	=> 'SNAS',
                   'parent_faculty_id'		=>  3,
	

        ]);

        DB::table('school')->insert([
                   'id'             => 10,
                   'school_name'  => 'School of Mathematics & Statistics',
                   'school_code'  => 'SNAS',
                   'parent_faculty_id'    =>  3
  

        ]);


         DB::table('school')->insert([
                   'id'             => 11,
                   'school_name'  => 'Caribbean School of Sports Sciences',
                   'school_code'  => 'CSSS',
                   'parent_faculty_id'    =>  3
  

        ]);


       DB::table('school')->insert([
             'id'             => 12,
             'school_name'  => 'Caribbean School of Sports Sciences',
             'school_code'  => 'CSSS',
             'parent_faculty_id'    =>  3,
  

        ]);

        DB::table('school')->insert([
             'id'             => 13,
             'school_name'  => 'Center for Science-based Research, Entrepreneurship & Continuing Studies',
             'school_code'  => 'CSSS',
             'parent_faculty_id'    =>  3,
  

        ]);

        DB::table('school')->insert([
             'id'             => 14,
             'school_name'  => 'School of Computing and Information Technology',
             'school_code'  => 'SCIT',
             'parent_faculty_id'    =>  5

        ]);

         DB::table('school')->insert([
             'id'             => 15,
             'school_name'  => 'School of Engineering',
             'school_code'  => 'ENGIN',
             'parent_faculty_id'    =>  5

        ]);


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		DB::table('school')->delete();
	}

}
