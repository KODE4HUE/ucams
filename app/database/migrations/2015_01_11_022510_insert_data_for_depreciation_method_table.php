<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Model\System\RoleManagement\Role;

class InsertDataForDepreciationMethodTable extends Migration 
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::table('depreciation_method')->insert([
                   'id'                         => 1,
                   'depreciation_method_name'   => 'Straight Line',
                    'depreciation_method_code'  => 'default-straight-line',
                ]);
                
                DB::table('depreciation_method')->insert([
                   'id'                         => 2,
                   'depreciation_method_name'   => 'Reducing Balance',
                    'depreciation_method_code'  => 'default-reducing-balance',
                ]);
              
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

            DB::table('depreciation_method')->delete(1);
            DB::table('depreciation_method')->delete(2);
	}

}
