<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeasedAssetTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
            Schema::create('leased_asset', function(Blueprint $table){
               $table->char('id', 36)->primary();
               $table->date('started_on');
               $table->date('expired_on')->nullable();
               $table->decimal('disposition_cost', 16, 4)->default(0);
               $table->decimal('monthly_payment', 16,4)->default(0);
               $table->char('asset_id', 36)->default(0);
               $table->text('additional_details');
               
                $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('updated_at')->default('0000-00-00 00:00:00');
                $table->softDeletes();

                $table->char('created_by_user_id', 36)->default('0');
                $table->char('updated_by_user_id', 36)->default('0');
                $table->char('deleted_by_user_id', 36)->default('0');
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
            Schema::drop('leased_asset');
	}

}
