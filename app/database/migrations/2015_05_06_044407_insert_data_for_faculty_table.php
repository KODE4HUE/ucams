<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertDataForFacultyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		DB::table('faculty')->insert([
                   'id'             => 1,
                   'faculty_name'	=> 'College of Health Sciences',
                   'faculty_code'	=> 'COHS'			

        ]);

        DB::table('faculty')->insert([
                   'id'             => 2,
                   'faculty_name'	=> 'College of Business and Management',
                   'faculty_code'	=> 'COBM'			

        ]);

        DB::table('faculty')->insert([
                   'id'             => 3,
                   'faculty_name'	=> 'Faculty of Education and Liberal Studies',
                   'faculty_code'	=> 'FELS'			

        ]);

        DB::table('faculty')->insert([
                   'id'             => 4,
                   'faculty_name'	=> 'Faculty of Engineering and Computing',
                   'faculty_code'	=> 'FENC'			

        ]);


        DB::table('faculty')->insert([
                   'id'             			=> 5,
                   'faculty_name'				=> 'Faculty of Engineering and Computing',
                   'faculty_code'				=> 'FENC'
        ]);


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		DB::table('faculty')->delete();
	}

}
