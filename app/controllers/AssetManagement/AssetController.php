<?php

use App\Services\Asset\AssetServiceManager;

class AssetController extends BaseController
{
    protected $restful = true;
    protected $assetServiceManager;

    public function __construct(AssetServiceManager $assetServiceManager)
    {
        $this->assetServiceManager = $assetServiceManager;
    }

    public function createAssetModel()
    {
        $statusCode = 200;

        $input =  Input::only('asset_model_id', 'asset_lifecycle_stages_id', 'serial_number', 'ledger_code', 'cost_price', 'current_value', 'total_tax',
            'utech_code', 'disposed_on', 'purchased_on', 'estimated_years_to_depreciate', 'is_disposed', 'date_disposed', 'additional_details', 'assigned_room_id',
            'assigned_to_staff_id', 'owner_school_id', 'agreement_type_id', 'transaction_id', 'created_by_user_id'

        );

        $this->assetServiceManager->createAsset($input);

        $result = $this->assetServiceManager->getResult();

        if(!$result['error'])
        {
            $statusCode = 200;
        }
        else
        {
            if($result['error-messages']['validation-messages'])
            {
                $statusCode = 400;
            }
            else
            {
                $statusCode = 500;
            }
        }

        return Response::json($result, $statusCode);
    }


    public function getAllAssets()
    {
        $statusCode = 200;

        $this->assetServiceManager->getAllStaff();

        $result = $this->assetServiceManager->getResult();

        dd($result);

        if(!$result['error'])
        {
            $statusCode = 200;
        }
        else
        {

            if($result['error-messages']['validation-messsages']){
                $statusCode = 400;
            }
            else
            {
                $statusCode = 500;
            }

        }

        return Response::json($result, $statusCode);
    }

}
