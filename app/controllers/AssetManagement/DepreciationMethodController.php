<?php

class DepreciationMethodController extends BaseController 
{
    protected $restful = true;
    protected $assetServiceManager;
    
    public function __construct(AssetServiceManager $assetServiceManager) {
        $this->assetServiceManager = $assetServiceManager;
    }
    
    
    public function getSelectable()
    {
        $statusCode = 200;
        
        $this->assetModelServiceManager->getDepreciationMethodSelectable();
        
        $result = $this->assetServiceManager->getResult();
        
        if(!$result['error'])
        {
            $statusCode = 200;
        }
        else
        {
            if($result['error-messages']['validation-messages']){
                $statusCode = 401;
            }
            if($result['error-messages']['not-found'])
            {
                $statusCode = 404;
            }
            else
            {
                $statusCode = 500;
            }
        }
        
        return Response::json($result, $statusCode);
    }   
}
